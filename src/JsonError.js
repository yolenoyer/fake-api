
/**
 * JsonError
 */
class JsonError {
    /**
     * Constructor.
     *
     * @param {int}    statusCode
     * @param {string} message
     */
    constructor(statusCode, message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    /**
     * @returns {object}
     */
    getJson() {
        return {
            success: false,
            message: this.message,
        };
    }
}

module.exports = {
    JsonError,
};
