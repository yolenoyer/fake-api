
const { LoremIpsum } = require('lorem-ipsum');


const types = [ 'word', 'sentence', 'paragraph' ];


/**
 * Lorem
 */
class Lorem {
    /**
     * Constructor.
     */
    constructor() {
        this.loremIpsum = new LoremIpsum();
    }

    /**
     * Generate lorem ipsum text.
     *
     * @param {int}    count How many words/sentences/paragraphs to generate
     * @param {string} type  'word|sentence|paragraph'
     *
     * @returns {undefined}
     */
    generate(count, type) {
        let output;

        switch (type) {
            case 'word':
                output = this.loremIpsum.generateWords(count);
                break;
            case 'sentence':
                output = this.loremIpsum.generateSentences(count);
                break;
            case 'paragraph':
                output = this.loremIpsum.generateParagraphs(count);
                break;
            default:
                throw 'Internal error';
        }

        return output;
    }
}

module.exports = {
    types,
    Lorem,
};
